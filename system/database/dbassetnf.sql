--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: master_asset; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.master_asset (
    kode character varying(10),
    nama character varying(50) NOT NULL,
    tgl_perolehan date,
    harga_perolehan double precision,
    kategori_id integer
);


ALTER TABLE public.master_asset OWNER TO postgres;

--
-- Name: barang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.barang (
    kategori_id integer DEFAULT 4,
    id integer NOT NULL,
    merk character varying(30),
    jenis_id integer
)
INHERITS (public.master_asset);


ALTER TABLE public.barang OWNER TO postgres;

--
-- Name: barang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.barang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.barang_id_seq OWNER TO postgres;

--
-- Name: barang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.barang_id_seq OWNED BY public.barang.id;


--
-- Name: departemen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departemen (
    id integer NOT NULL,
    nama character varying(100),
    abbr character varying(15),
    alamat character varying(100),
    telpon character varying(20)
);


ALTER TABLE public.departemen OWNER TO postgres;

--
-- Name: departemen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.departemen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departemen_id_seq OWNER TO postgres;

--
-- Name: departemen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.departemen_id_seq OWNED BY public.departemen.id;


--
-- Name: gedung; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gedung (
    kategori_id integer DEFAULT 1,
    id integer NOT NULL,
    alamat character varying(100),
    latlong character varying(30),
    jml_lantai integer
)
INHERITS (public.master_asset);


ALTER TABLE public.gedung OWNER TO postgres;

--
-- Name: gedung_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gedung_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gedung_id_seq OWNER TO postgres;

--
-- Name: gedung_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gedung_id_seq OWNED BY public.gedung.id;


--
-- Name: jenis_barang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jenis_barang (
    id integer NOT NULL,
    nama character varying(30)
);


ALTER TABLE public.jenis_barang OWNER TO postgres;

--
-- Name: jenis_barang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jenis_barang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_barang_id_seq OWNER TO postgres;

--
-- Name: jenis_barang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jenis_barang_id_seq OWNED BY public.jenis_barang.id;


--
-- Name: jenis_kendaraan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jenis_kendaraan (
    id integer NOT NULL,
    nama character varying(30)
);


ALTER TABLE public.jenis_kendaraan OWNER TO postgres;

--
-- Name: jenis_kendaraan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jenis_kendaraan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_kendaraan_id_seq OWNER TO postgres;

--
-- Name: jenis_kendaraan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jenis_kendaraan_id_seq OWNED BY public.jenis_kendaraan.id;


--
-- Name: jenis_ruangan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jenis_ruangan (
    id integer NOT NULL,
    nama character varying(30)
);


ALTER TABLE public.jenis_ruangan OWNER TO postgres;

--
-- Name: jenis_ruangan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jenis_ruangan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_ruangan_id_seq OWNER TO postgres;

--
-- Name: jenis_ruangan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jenis_ruangan_id_seq OWNED BY public.jenis_ruangan.id;


--
-- Name: kategori; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kategori (
    id integer NOT NULL,
    nama character varying(30)
);


ALTER TABLE public.kategori OWNER TO postgres;

--
-- Name: kategori_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kategori_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kategori_id_seq OWNER TO postgres;

--
-- Name: kategori_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kategori_id_seq OWNED BY public.kategori.id;


--
-- Name: kendaraan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kendaraan (
    kategori_id integer DEFAULT 2,
    id integer NOT NULL,
    nopol character varying(20),
    merk character varying(30),
    jenis_id integer
)
INHERITS (public.master_asset);


ALTER TABLE public.kendaraan OWNER TO postgres;

--
-- Name: kendaraan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kendaraan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kendaraan_id_seq OWNER TO postgres;

--
-- Name: kendaraan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kendaraan_id_seq OWNED BY public.kendaraan.id;


--
-- Name: pegawai; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pegawai (
    id integer NOT NULL,
    nip character varying(20),
    nama character varying(50) NOT NULL,
    gender character(1),
    tmp_lahir character varying(30),
    tgl_lahir date,
    alamat character varying(100),
    dep_id integer
);


ALTER TABLE public.pegawai OWNER TO postgres;

--
-- Name: pegawai_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pegawai_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pegawai_id_seq OWNER TO postgres;

--
-- Name: pegawai_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pegawai_id_seq OWNED BY public.pegawai.id;


--
-- Name: pinjam_kendaraan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pinjam_kendaraan (
    id integer NOT NULL,
    tgl_pinjam date,
    tgl_kembali date,
    keperluan character varying(100),
    pegawai_id integer,
    status_pinjam smallint,
    berita character varying(200)
);


ALTER TABLE public.pinjam_kendaraan OWNER TO postgres;

--
-- Name: pinjam_kendaraan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pinjam_kendaraan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pinjam_kendaraan_id_seq OWNER TO postgres;

--
-- Name: pinjam_kendaraan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pinjam_kendaraan_id_seq OWNED BY public.pinjam_kendaraan.id;


--
-- Name: ruangan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ruangan (
    id integer NOT NULL,
    gedung_id integer,
    lantai smallint,
    kapasitas smallint,
    nama character varying(40),
    jenis_id integer
);


ALTER TABLE public.ruangan OWNER TO postgres;

--
-- Name: ruangan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ruangan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ruangan_id_seq OWNER TO postgres;

--
-- Name: ruangan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ruangan_id_seq OWNED BY public.ruangan.id;


--
-- Name: tanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tanah (
    kategori_id integer DEFAULT 3,
    id integer NOT NULL,
    luas double precision,
    alamat character varying(100),
    latlong character varying(30),
    nosertifikat character varying(30)
)
INHERITS (public.master_asset);


ALTER TABLE public.tanah OWNER TO postgres;

--
-- Name: tanah_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tanah_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tanah_id_seq OWNER TO postgres;

--
-- Name: tanah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tanah_id_seq OWNED BY public.tanah.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(20),
    password character varying(100),
    role character varying(15)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: barang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barang ALTER COLUMN id SET DEFAULT nextval('public.barang_id_seq'::regclass);


--
-- Name: departemen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departemen ALTER COLUMN id SET DEFAULT nextval('public.departemen_id_seq'::regclass);


--
-- Name: gedung id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gedung ALTER COLUMN id SET DEFAULT nextval('public.gedung_id_seq'::regclass);


--
-- Name: jenis_barang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_barang ALTER COLUMN id SET DEFAULT nextval('public.jenis_barang_id_seq'::regclass);


--
-- Name: jenis_kendaraan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_kendaraan ALTER COLUMN id SET DEFAULT nextval('public.jenis_kendaraan_id_seq'::regclass);


--
-- Name: jenis_ruangan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_ruangan ALTER COLUMN id SET DEFAULT nextval('public.jenis_ruangan_id_seq'::regclass);


--
-- Name: kategori id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kategori ALTER COLUMN id SET DEFAULT nextval('public.kategori_id_seq'::regclass);


--
-- Name: kendaraan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kendaraan ALTER COLUMN id SET DEFAULT nextval('public.kendaraan_id_seq'::regclass);


--
-- Name: pegawai id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pegawai ALTER COLUMN id SET DEFAULT nextval('public.pegawai_id_seq'::regclass);


--
-- Name: pinjam_kendaraan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_kendaraan ALTER COLUMN id SET DEFAULT nextval('public.pinjam_kendaraan_id_seq'::regclass);


--
-- Name: ruangan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ruangan ALTER COLUMN id SET DEFAULT nextval('public.ruangan_id_seq'::regclass);


--
-- Name: tanah id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanah ALTER COLUMN id SET DEFAULT nextval('public.tanah_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: barang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.barang (kode, nama, tgl_perolehan, harga_perolehan, kategori_id, id, merk, jenis_id) FROM stdin;
\.


--
-- Data for Name: departemen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.departemen (id, nama, abbr, alamat, telpon) FROM stdin;
1	Dosen	abbr	Jakarta	02100011
2	Security	abbr\n	Bandung	02130090
\.


--
-- Data for Name: gedung; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gedung (kode, nama, tgl_perolehan, harga_perolehan, kategori_id, id, alamat, latlong, jml_lantai) FROM stdin;
\.


--
-- Data for Name: jenis_barang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jenis_barang (id, nama) FROM stdin;
1	Laptop
2	PC
3	LCD Monitor
4	TV / SmartTV
5	LCD Projector
6	Lemari
7	Meja/Kursi
\.


--
-- Data for Name: jenis_kendaraan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jenis_kendaraan (id, nama) FROM stdin;
1	Sepeda
2	Motor
3	Mobil
\.


--
-- Data for Name: jenis_ruangan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jenis_ruangan (id, nama) FROM stdin;
1	Aula
2	Ruang Rapat
3	Mushola
4	Ruang Tamu
5	Toilets
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kategori (id, nama) FROM stdin;
1	Gedung
3	Tanah
4	Barang
2	Kendaraan
\.


--
-- Data for Name: kendaraan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kendaraan (kode, nama, tgl_perolehan, harga_perolehan, kategori_id, id, nopol, merk, jenis_id) FROM stdin;
\.


--
-- Data for Name: master_asset; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.master_asset (kode, nama, tgl_perolehan, harga_perolehan, kategori_id) FROM stdin;
\.


--
-- Data for Name: pegawai; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pegawai (id, nip, nama, gender, tmp_lahir, tgl_lahir, alamat, dep_id) FROM stdin;
9	0110218042	Junaedi	L	Jakarta	2019-12-01	Jalan kecapi I no 52 C2	1
26	12345678	Sinta Jojo	L	Jakarta	2019-01-07	Jalan kecapi I no 52 C2	1
4	0110218040	Firman Permana	P	Garut	1995-10-10	Jakarta	1
22	01102180456	Eko Depa	L	Jakarta	2019-12-12	Tg. Perak - Surabaya	1
40	123	Iqro	L	Garut	2020-01-23	Jalan kecapi I no 52 C2	1
\.


--
-- Data for Name: pinjam_kendaraan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pinjam_kendaraan (id, tgl_pinjam, tgl_kembali, keperluan, pegawai_id, status_pinjam, berita) FROM stdin;
7	2020-01-22	2020-01-29	keperluan	9	2	Berjalan
8	2020-01-15	2020-01-16	Untuk jalan jalan bersama keluarga	26	1	Pada tanggal berapa
\.


--
-- Data for Name: ruangan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ruangan (id, gedung_id, lantai, kapasitas, nama, jenis_id) FROM stdin;
\.


--
-- Data for Name: tanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tanah (kode, nama, tgl_perolehan, harga_perolehan, kategori_id, id, luas, alamat, latlong, nosertifikat) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, username, password, role) FROM stdin;
1	admin	827ccb0eea8a706c4c34a16891f84e7b	admin
2	staf	827ccb0eea8a706c4c34a16891f84e7b	staf
\.


--
-- Name: barang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.barang_id_seq', 1, false);


--
-- Name: departemen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.departemen_id_seq', 1, true);


--
-- Name: gedung_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gedung_id_seq', 1, false);


--
-- Name: jenis_barang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jenis_barang_id_seq', 7, true);


--
-- Name: jenis_kendaraan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jenis_kendaraan_id_seq', 3, true);


--
-- Name: jenis_ruangan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jenis_ruangan_id_seq', 5, true);


--
-- Name: kategori_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kategori_id_seq', 4, true);


--
-- Name: kendaraan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kendaraan_id_seq', 1, false);


--
-- Name: pegawai_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pegawai_id_seq', 42, true);


--
-- Name: pinjam_kendaraan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pinjam_kendaraan_id_seq', 8, true);


--
-- Name: ruangan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ruangan_id_seq', 1, false);


--
-- Name: tanah_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tanah_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id);


--
-- Name: departemen departemen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departemen
    ADD CONSTRAINT departemen_pkey PRIMARY KEY (id);


--
-- Name: gedung gedung_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gedung
    ADD CONSTRAINT gedung_pkey PRIMARY KEY (id);


--
-- Name: jenis_barang jenis_barang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_barang
    ADD CONSTRAINT jenis_barang_pkey PRIMARY KEY (id);


--
-- Name: jenis_kendaraan jenis_kendaraan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_kendaraan
    ADD CONSTRAINT jenis_kendaraan_pkey PRIMARY KEY (id);


--
-- Name: jenis_ruangan jenis_ruangan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_ruangan
    ADD CONSTRAINT jenis_ruangan_pkey PRIMARY KEY (id);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (id);


--
-- Name: kendaraan kendaraan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kendaraan
    ADD CONSTRAINT kendaraan_pkey PRIMARY KEY (id);


--
-- Name: master_asset master_asset_kode_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_asset
    ADD CONSTRAINT master_asset_kode_key UNIQUE (kode);


--
-- Name: pegawai pegawai_nip_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pegawai
    ADD CONSTRAINT pegawai_nip_key UNIQUE (nip);


--
-- Name: pegawai pegawai_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pegawai
    ADD CONSTRAINT pegawai_pkey PRIMARY KEY (id);


--
-- Name: pinjam_kendaraan pinjam_kendaraan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_kendaraan
    ADD CONSTRAINT pinjam_kendaraan_pkey PRIMARY KEY (id);


--
-- Name: ruangan ruangan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ruangan
    ADD CONSTRAINT ruangan_pkey PRIMARY KEY (id);


--
-- Name: tanah tanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tanah
    ADD CONSTRAINT tanah_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: barang barang_jenis_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_jenis_id_fkey FOREIGN KEY (jenis_id) REFERENCES public.jenis_barang(id);


--
-- Name: kendaraan kendaraan_jenis_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kendaraan
    ADD CONSTRAINT kendaraan_jenis_id_fkey FOREIGN KEY (jenis_id) REFERENCES public.jenis_kendaraan(id);


--
-- Name: master_asset master_asset_kategori_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.master_asset
    ADD CONSTRAINT master_asset_kategori_id_fkey FOREIGN KEY (kategori_id) REFERENCES public.kategori(id);


--
-- Name: pegawai pegawai_dep_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pegawai
    ADD CONSTRAINT pegawai_dep_id_fkey FOREIGN KEY (dep_id) REFERENCES public.departemen(id);


--
-- Name: pinjam_kendaraan pinjam_kendaraan_pegawai_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_kendaraan
    ADD CONSTRAINT pinjam_kendaraan_pegawai_id_fkey FOREIGN KEY (pegawai_id) REFERENCES public.pegawai(id);


--
-- Name: ruangan ruangan_gedung_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ruangan
    ADD CONSTRAINT ruangan_gedung_id_fkey FOREIGN KEY (gedung_id) REFERENCES public.gedung(id);


--
-- Name: ruangan ruangan_jenis_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ruangan
    ADD CONSTRAINT ruangan_jenis_id_fkey FOREIGN KEY (jenis_id) REFERENCES public.jenis_ruangan(id);


--
-- PostgreSQL database dump complete
--

