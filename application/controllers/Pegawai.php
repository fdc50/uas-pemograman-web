<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

     public function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('logged') == NULL) {
        header("Location:" . site_url('/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
     }
    $this->load->model('ModelPegawai');
    $this->load->helper(array('form', 'url'));
    
  }
	public function index()
	{
        
        $data['title']= 'Pegawai';
        $data['pegawais']= $this->ModelPegawai->getdata();
        
        $this->load->view('pegawai/index', $data);
    }
    
    public function create()
    {
        $data['deppegawai'] = $this->ModelPegawai->get_dep_id();
        $this->load->view('pegawai/create',$data); 
       
    }

    public function add()
    {
        $nip= $this->input->post('nip');
        $nama= $this->input->post('nama');
        $gender= $this->input->post('gender');
        $alamat= $this->input->post('alamat');
        $tmp_lahir= $this->input->post('tmp_lahir');
        $tgl_lahir= $this->input->post('tgl_lahir');
        $dep_id= $this->input->post('dep_id');

        $data= array(
            'nama' => $nama,
            'nip' => $nip,
            'gender' => $gender,
            'alamat' => $alamat,
            'tmp_lahir' => $tmp_lahir,
            'tgl_lahir' => $tgl_lahir,
            'dep_id' => $dep_id
        );

        
        
        $this->ModelPegawai->inputdata($data,'pegawai');
        redirect('pegawai/index');
    }

    function edit($id)
    {              
        $where = array('id' => $id);
        $data['deppegawai'] = $this->ModelPegawai->get_dep_id();
        $data['pegawai'] = $this->ModelPegawai->editdata($where,'pegawai','departemen')->result();
       
        $this->load->view('pegawai/edit',$data);
    }

    function update()
    {
        $id = $this->input->post('id');
        $nip= $this->input->post('nip');
        $nama= $this->input->post('nama');
        $gender= $this->input->post('gender');
        $alamat= $this->input->post('alamat');
        $tmp_lahir= $this->input->post('tmp_lahir');
        $tgl_lahir= $this->input->post('tgl_lahir');
        $dep_id= $this->input->post('dep_id');

        $data= array(
            'nama' => $nama,
            'nip' => $nip,
            'gender' => $gender,
            'alamat' => $alamat,
            'tmp_lahir' => $tmp_lahir,
            'tgl_lahir' => $tgl_lahir,
            'dep_id' => $dep_id
        );

        $where = array('id' => $id);
        
        $this->ModelPegawai->updatedata($where,$data,'pegawai');
        redirect('pegawai/index');
    }

    function view($id)
    {              
        $where = array('id' => $id);
        $data['pegawai'] = $this->ModelPegawai->view($where,'pegawai')->row_array();
        $data['deppegawai'] = $this->ModelPegawai->get_dep_id(array('id'=>$data['pegawai']['dep_id']));
        // print_r($data['deppegawai']);
        // die();
        
        

        $pegawai = $data;

        // var_dump($pegawai);
        // die();

        
        $this->load->view('pegawai/view',$pegawai);
    }

    function delete($id)
    {
        $where = array('id' => $id);
        $this->ModelPegawai->delete($where, 'pegawai');
        $this->session->set_flashdata('message', 'Data berhasil dihapus');
        redirect('pegawai/index');
    }
}
