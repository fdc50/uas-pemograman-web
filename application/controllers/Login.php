<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	 public function __construct()
  {
    parent::__construct();
	$this->load->model('ModelUser');
	$this->load->library('form_validation');
	$this->load->helper('string');
  }
	public function index()
	{
		$this->load->view('authentication/login');
	}

	function login()
	{
		if ($this->session->userdata('logged')) {
			redirect('dashboard');
		}

		if ($this->input->post('location')) {
			$location = $this->input->post('location');
		}else{
			$location = NULL;
		}

		// print_r($this->input->post());
		// die();

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($_POST AND $this->form_validation->run() == TRUE) {
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);

			

			$user = $this->ModelUser->get(array('username' => $username, 'password' => md5($password)));
			
						
			if (count($user) > 0) {
                $this->session->set_userdata('logged', TRUE);
				$this->session->set_userdata('uid', $user[0]['id']);
				$this->session->set_userdata('username', $user[0]['username']);
				$this->session->set_userdata('uroleid', $user[0]['role']);
                if ($location != '') {
                    header("Location:" . htmlspecialchars($location));
                } else {
                    redirect('dashboard');
                }
            } else {
                if ($location != '') {
                    $this->session->set_flashdata('failed', 'Maaf, username dan password tidak cocok!');
                    header("Location:" . site_url('login') . "?location=" . urlencode($location));
                } else {
                    $this->session->set_flashdata('failed', 'Maaf, username dan password tidak cocok!');
                    redirect('login');
                }
            }
        } else {
            $this->load->view('login');
        }	

	}

	// Logout Processing
    function logout() {
        $this->session->unset_userdata('logged');
        $this->session->unset_userdata('uid');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('uroleid');
        $this->session->unset_userdata('role');

        $q = $this->input->get(NULL, TRUE);
        if ($q['location'] != NULL) {
            $location = $q['location'];
        } else {
            $location = NULL;
        }
        header("Location:" . $location);
    }
}
