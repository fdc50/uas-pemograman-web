<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PinjamKendaraan extends CI_Controller {

	 public function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('logged') == NULL) {
        header("Location:" . site_url('/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
     }
    $this->load->model('ModelPinjamKendaraan');
    $this->load->helper(array('form', 'url'));
  }
	public function index()
	{
		$data['title']= 'Pegawai';
		$data['pinjams']= $this->ModelPinjamKendaraan->getdata();
		
		$this->load->view('pinjamkendaraan/index',$data);
	}

	public function create()
	{
		$data['idpegawai'] = $this->ModelPinjamKendaraan->get_peg_id();
        $this->load->view('pinjamkendaraan/create',$data); 
	}

	public function add()
    {
        $tgl_pinjam= $this->input->post('tgl_pinjam');
        $tgl_kembali= $this->input->post('tgl_kembali');
        $keperluan= $this->input->post('keperluan');
        $pegawai_id= $this->input->post('pegawai_id');
        $status_pinjam= $this->input->post('status_pinjam');
        $berita= $this->input->post('berita');

        $data= array(
            'tgl_pinjam' => $tgl_pinjam,
            'tgl_kembali' => $tgl_kembali,
            'keperluan' => $keperluan,
            'pegawai_id' => $pegawai_id,
            'status_pinjam' => $status_pinjam,
            'berita' => $berita
		);
		        
        $this->ModelPinjamKendaraan->inputdata($data,'pinjam_kendaraan');
        redirect('pinjamkendaraan/index');
	}

	function edit($id)
    {              
        $where = array('id' => $id);
        $data['pinjam'] = $this->ModelPinjamKendaraan->view($where,'pinjam_kendaraan')->row_array();
		$data['pegawaiid'] = $this->ModelPinjamKendaraan->get_peg_id();
		
		// print_r($data['pegawaiid']);
		// die();

        $pinjam = $data;
       
        $this->load->view('pinjamkendaraan/edit',$data);
    }

    function update()
    {
		$id = $this->input->post('id');
		$tgl_pinjam= $this->input->post('tgl_pinjam');
        $tgl_kembali= $this->input->post('tgl_kembali');
        $keperluan= $this->input->post('keperluan');
        $pegawai_id= $this->input->post('pegawai_id');
        $status_pinjam= $this->input->post('status_pinjam');
        $berita= $this->input->post('berita');

        $data= array(
            'tgl_pinjam' => $tgl_pinjam,
            'tgl_kembali' => $tgl_kembali,
            'keperluan' => $keperluan,
            'pegawai_id' => $pegawai_id,
            'status_pinjam' => $status_pinjam,
            'berita' => $berita
		);

		
        $where = array('id' => $id);
        
        $this->ModelPinjamKendaraan->updatedata($where,$data,'pinjam_kendaraan');
        redirect('pinjamkendaraan/index');
    }
	
	function view($id)
    {              
        $where = array('id' => $id);
        $data['pinjam'] = $this->ModelPinjamKendaraan->view($where,'pinjam_kendaraan')->row_array();
        $data['pegawaiid'] = $this->ModelPinjamKendaraan->get_peg_id(array('id'=>$data['pinjam']['pegawai_id']));

        $pinjam = $data;
        
        $this->load->view('pinjamkendaraan/view',$pinjam);
	}
	
	function delete($id)
    {
        $where = array('id' => $id);
        $this->ModelPinjamKendaraan->delete($where, 'pinjam_kendaraan');
        $this->session->set_flashdata('message', 'Data berhasil dihapus');
        redirect('pinjamkendaraan/index');
    }
}
