<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	 public function __construct()
  {
	parent::__construct();
	if ($this->session->userdata('logged') == NULL) {
        header("Location:" . site_url('/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
	 }
	 $this->load->model('ModelPegawai');
	 $this->load->helper(array('form', 'url')); 
    
  }
	public function index()
	{
		$data['title']= 'Pegawai';
        $data['pegawais']= $this->ModelPegawai->getdata();
		$this->load->view('authentication/dashboard');
	}
}
