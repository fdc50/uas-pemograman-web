<?php $this->load->view('template/header'); ?>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tambah Pegawai</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Pegawai</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  action="<?php echo base_url(). 'index.php/pegawai/add'; ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="kfk kkk">NIP</label>
                    <input type="text" class="form-control" name="nip"  placeholder="NIP">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label for="tempatlahir">Tempat Lahir</label>
                    <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir">
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Lahir">Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir" placeholder="Tanggal Lahir">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" name="alamat" placeholder="Alamat">
                  </div>
                  <div class="form-group">
                    <label >Jenis Kelamin</label>
                    <div class="form-group">
                    <label>
                        <input type="radio" name="gender" id="laki-laki" value="L"> Laki-Laki
                    </label>
                    <label>
                        <input type="radio" name="gender" id="perempuan" value="P" > Perempuan
                    </label>
                </div>
                <div class="form-group">
                  <label >Departemen</label>
                  <select name="dep_id" class="form-control">
                  <option value="">-- Pilih Departemen --</option>
                  <?php foreach ($deppegawai as $key): ?>
                  <option value="<?php echo $key['id']; ?>" ><?php echo $key['nama']; ?></option>
                  <?php endforeach; ?>
                  </select>
                 </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Foto</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button> <a href="<?php echo site_url('pegawai') ?>" class="btn btn-warning btn-xxs pull-right"><i class="fa fa-arrow-left"></i> Batal</a>
                </div>
              </form>
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>