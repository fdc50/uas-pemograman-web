<?php $this->load->view('template/header'); ?>
<?php //echo $this->session->flashdata('message');?>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Pegawai </h3>
              <div align="right">
              <a href="<?php echo site_url('pegawai/create') ?>" class="btn btn-success btn-xxs pull-right"><i class="fas fa-plus"></i> Tambah</a>
              </div>
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Alamat</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                if(!empty($pegawais)) {
                    foreach ($pegawais as $row):
                        ?>  
                <tr>
                    <td><?php echo $row->nip; ?> </td>
                    <td><?php echo $row->nama; ?> </td>
                    <td><?php if($row->gender=='L'){ echo 'Laki-laki'; }else{
                            echo 'Perempuan';
                        } ?>
                    </td>
                    <td><?php echo $row->alamat; ?> </td>
                    <td>
                        <?php echo anchor('pegawai/view/'.$row->id, '<button type="button" class="btn btn-primary float-center"><i class="fas fa-eye"></i></button>') ?>
                        <?php echo anchor('pegawai/edit/'.$row->id, '<button type="button" class="btn btn-info float-center"><i class="fas fa-edit"></i></button>') ?>
                        <?php echo anchor('pegawai/delete/'.$row->id, '<button type="button" class="btn btn-danger float-center"><i class="fas fa-trash"></i></button>') ?>
                    </td>
                </tr>
                <?php 
                endforeach;
                    }else{
                ?>
                <tr id="row">
                    <td colspan="5" align="center">Data Kosong</td>
                </tr>
                <?php } ?>              

              </table>
            
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>