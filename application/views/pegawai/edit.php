<?php $this->load->view('template/header'); ?>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Pegawai</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Pegawai</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php foreach($pegawai as $row) ?>
              <form  action="<?php echo base_url(). 'index.php/pegawai/update'; ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                  </div>
                  <div class="form-group">
                    <label for="nip">NIP</label>
                    <input type="text" class="form-control" name="nip"  placeholder="NIP" value="<?php echo $row->nip; ?>">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" value="<?php echo $row->nama; ?>">
                  </div>
                  <div class="form-group">
                    <label for="tempatlahir">Tempat Lahir</label>
                    <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="<?php echo $row->tmp_lahir; ?>">
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Lahir">Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?php echo $row->tgl_lahir; ?>">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?php echo $row->alamat; ?>">
                  </div>
                  <label >Jenis Kelamin</label>
                  <div class="form-group">
                    <label>
                        <input type="radio" name="gender" id="laki-laki" value="L" <?php echo $row->gender=='L'?'checked':'' ?>> Laki-Laki
                    </label>
                    <label>
                        <input type="radio" name="gender" id="perempuan" value="P" <?php echo $row->gender=='P'?'checked':'' ?>> Perempuan
                    </label>
                </div>
                <div class="form-group">
                  <label >Departemen</label>
                  <select name="dep_id" class="form-control">
						<option value="">-- Pilih Departemen --</option>
						<?php foreach ($idpegawai as $key): ?>
						<option value="<?php echo $key['id']; ?>" <?php echo isset($row) &&  $key['id'] == $row->dep_id  ? 'selected' : '' ?>><?php echo $key['nama']; ?></option>
						<?php endforeach; ?>
					</select>
                 </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputFile">Foto</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div> -->
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button> <a href="<?php echo site_url('pegawai') ?>" class="btn btn-warning btn-xxs pull-right"><i class="fa fa-arrow-left"></i> Batal</a>
                </div>
              </form>
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>