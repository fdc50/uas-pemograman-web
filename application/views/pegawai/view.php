<?php $this->load->view('template/header'); ?>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detail Pegawai</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Nama</th>
                        <td>:</td>
                        <td><?php echo $pegawai['nama'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">NIP</th>
                        <td>:</td>
                        <td><?php echo $pegawai['nip'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Alamat</th>
                        <td>:</td>
                        <td><?php echo $pegawai['alamat'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Jenis Kelamin</th>
                        <td>:</td>
                        <td><?php if($pegawai['gender']=='L'){ echo 'Laki-laki'; }else{
                            echo 'Perempuan';
                        } ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Tempat Lahir</th>
                        <td>:</td>
                        <td><?php echo $pegawai['tmp_lahir'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Tanggal Lahir</th>
                        <td>:</td>
                        <td><?php echo $pegawai['tgl_lahir'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Departemen</th>
                        <td>:</td>
                        <td>
                          <?php echo $deppegawai['nama'] ?>                           
                        </td>
                      </tr>
                    </table>
                  </div>
                  <a href="<?php echo site_url('pegawai') ?>" class="btn btn-warning btn-xxs pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>