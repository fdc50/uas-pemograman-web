<?php $this->load->view('template/header'); ?>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detail Peminjaman</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="table-responsive">
                    <table class="table">
                     <tr>
                        <th style="width:50%">Nama Peminjam</th>
                        <td>:</td>
                        <td><?php echo $pegawaiid['nama'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Tanggal Pinjam</th>
                        <td>:</td>
                        <td><?php echo $pinjam['tgl_pinjam'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Tanggal Pengembalian</th>
                        <td>:</td>
                        <td><?php echo $pinjam['tgl_kembali'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Keperluan</th>
                        <td>:</td>
                        <td><?php echo $pinjam['keperluan'] ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Status Pinjam</th>
                        <td>:</td>
                        <td><?php if($pinjam['status_pinjam']=='1'){ echo 'Sewa'; }elseif($pinjam['status_pinjam']=='2'){
                            echo 'Pinjam Pakai';
                        }else{
                            echo 'Jajang';
                        } ?></td>
                      </tr>
                      <tr>
                        <th style="width:50%">Berita</th>
                        <td>:</td>
                        <td><?php echo $pinjam['berita'] ?></td>
                      </tr>
                    </table>
                  </div>
                  <a href="<?php echo site_url('pinjamkendaraan') ?>" class="btn btn-warning btn-xxs pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>