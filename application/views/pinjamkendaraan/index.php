<?php $this->load->view('template/header'); ?>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Peminjaman Kendaraan </h3>
              <div align="right">
              <a href="<?php echo site_url('pinjamkendaraan/create') ?>" class="btn btn-success btn-xxs pull-right"><i class="fas fa-plus"></i> Tambah</a>
              </div>
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tanggal Pinjam</th>
                  <th>Tanggal Pengembalian</th>
                  <th>Keperluan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                if(!empty($pinjams)) {
                    foreach ($pinjams as $row):
                        ?>  
                <tr>
                    <td><?php echo $row->tgl_pinjam; ?> </td>
                    <td><?php echo $row->tgl_kembali; ?> </td>
                    <td><?php echo $row->keperluan; ?> </td>
                    <td>
                        <?php echo anchor('pinjamkendaraan/view/'.$row->id, '<button type="button" class="btn btn-primary float-center"><i class="fas fa-eye"></i></button>') ?>
                        <?php echo anchor('pinjamkendaraan/edit/'.$row->id, '<button type="button" class="btn btn-info float-center"><i class="fas fa-edit"></i></button>') ?>
                        <?php echo anchor('pinjamkendaraan/delete/'.$row->id, '<button type="button" class="btn btn-danger float-center"><i class="fas fa-trash"></i></button>') ?>
                    </td>
                </tr>
                <?php 
                endforeach;
                    }else{
                ?>
                <tr id="row">
                    <td colspan="5" align="center">Data Kosong</td>
                </tr>
                <?php } ?>              

              </table>
            
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>