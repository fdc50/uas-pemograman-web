<?php $this->load->view('template/header'); ?>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Pinjam Kendaraan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Pinjam Kendaraan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  action="<?php echo base_url(). 'index.php/pinjamkendaraan/update'; ?>" method="post">
                <div class="card-body">
                <div class="form-group">
                    <input type="hidden" name="id" value="<?php echo $pinjam['id']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Pinjam">Tanggal Pinjam</label>
                    <input type="date" class="form-control" name="tgl_pinjam"  placeholder="Tanggal Pinjam" value="<?php echo $pinjam['tgl_pinjam'] ?>" >
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Pengembalian">Tanggal Pengembalian</label>
                    <input type="date" class="form-control" name="tgl_kembali"  placeholder="Tanggal Pengembalian" value="<?php echo $pinjam['tgl_kembali'] ?>">
                  </div>
                  <div class="form-group">
                    <label for="nama">Keperluan</label>
                    <input type="text" class="form-control" name="keperluan" placeholder="Keperluan" value="<?php echo $pinjam['keperluan'] ?>">
                  </div>
                  <div class="form-group">
                  <label >Nama Peminjam</label>
                  <select name="pegawai_id" class="form-control">
                  <option value="">-- Pilih Peminjam --</option>
                  <?php foreach ($pegawaiid as $key): ?>
                    <option value="<?php echo $key['id']; ?>" <?php echo isset($pinjam) &&  $key['id'] == $pinjam['pegawai_id']  ? 'selected' : '' ?>><?php echo $key['nama']; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </div>
                  <div class="form-group">
                    <label >Status Pinjam</label>
                    <div class="form-group">
                    <label>
                        <input type="radio" name="status_pinjam" id="laki-laki" value="1" <?php echo $pinjam['status_pinjam'] =='1'?'checked':'' ?>> Sewa
                    </label>
                    <label>
                        <input type="radio" name="status_pinjam" id="perempuan" value="2" <?php echo $pinjam['status_pinjam'] =='2'?'checked':'' ?> > Pinjam Pakai
                    </label>
                    <label>
                        <input type="radio" name="status_pinjam" id="perempuan" value="3" <?php echo $pinjam['status_pinjam'] =='3'?'checked':'' ?> > Kerjasama Pemanfaatan
                    </label>
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Lahir">Berita</label>
                    <input type="text" class="form-control" name="berita" placeholder="Berita" value="<?php echo $pinjam['berita'] ?>" >
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button> <a href="<?php echo site_url('pinjamkendaraan') ?>" class="btn btn-warning btn-xxs pull-right"><i class="fa fa-arrow-left"></i> Batal</a>
                </div>
              </form>
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>