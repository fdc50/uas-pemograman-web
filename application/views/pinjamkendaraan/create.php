<?php $this->load->view('template/header'); ?>
 <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tambah Pinjam Kendaraan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Pinjam</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  action="<?php echo base_url(). 'index.php/pinjamkendaraan/add'; ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="Tanggal Pinjam">Tanggal Pinjam</label>
                    <input type="date" class="form-control" name="tgl_pinjam"  placeholder="Tanggal Pinjam">
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Pengembalian">Tanggal Pengembalian</label>
                    <input type="date" class="form-control" name="tgl_kembali"  placeholder="Tanggal Pengembalian">
                  </div>
                  <div class="form-group">
                    <label for="nama">Keperluan</label>
                    <input type="text" class="form-control" name="keperluan" placeholder="Keperluan">
                  </div>
                  <div class="form-group">
                  <label >Nama Peminjam</label>
                  <select name="pegawai_id" class="form-control">
                  <option value="">-- Pilih Peminjam --</option>
                  <?php foreach ($idpegawai as $key): ?>
                  <option value="<?php echo $key['id']; ?>" ><?php echo $key['nama']; ?></option>
                  <?php endforeach; ?>
                  </select>
                  </div>
                  <div class="form-group">
                    <label >Status Pinjam</label>
                    <div class="form-group">
                    <label>
                        <input type="radio" name="status_pinjam" id="laki-laki" value="1"> Sewa
                    </label>
                    <label>
                        <input type="radio" name="status_pinjam" id="perempuan" value="2" > Pinjam Pakai
                    </label>
                    <label>
                        <input type="radio" name="status_pinjam" id="perempuan" value="3" > Kerjasama Pemanfaatan
                    </label>
                  </div>
                  <div class="form-group">
                    <label for="Tanggal Lahir">Berita</label>
                    <input type="text" class="form-control" name="berita" placeholder="Berita">
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button> <a href="<?php echo site_url('pinjamkendaraan') ?>" class="btn btn-warning btn-xxs pull-right"><i class="fa fa-arrow-left"></i> Batal</a>
                </div>
              </form>
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  
 <?php $this->load->view('template/footer'); ?>