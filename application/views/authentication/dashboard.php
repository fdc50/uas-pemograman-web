<?php $this->load->view('template/header'); ?>
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <!-- USERS LIST -->
        <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Kelompok 6</h3>

                    <div class="card-tools">
                      <span class="badge badge-danger">Our Members</span>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <ul class="users-list clearfix">
                      <li>
                        <img src="<?php echo base_url();?>assets/images/pp.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Firman Permana</a>
                        <span class="users-list-date">0110218040</span>
                      </li>
                      <li>
                        <img src="<?php echo base_url();?>assets/images/pp.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Anggi Yudha P</a>
                        <span class="users-list-date">0110218040</span>
                      </li>
                      <li>
                        <img src="<?php echo base_url();?>assets/images/pp.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Eko Depa</a>
                        <span class="users-list-date">0110218040</span>
                      </li>
                      <li>
                        <img src="<?php echo base_url();?>assets/images/pp.jpg" alt="User Image">
                        <a class="users-list-name" href="#">Budi Setiawan</a>
                        <span class="users-list-date">0110218040</span>
                      </li>
                      
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer text-center">
                    <a href="javascript::">View All Users</a>
                  </div>
                  <!-- /.card-footer -->
                </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
<?php $this->load->view('template/footer'); ?>
  
