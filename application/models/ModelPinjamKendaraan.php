<?php

class ModelPinjamKendaraan extends CI_Model{
    function __construct(){
        parent::__construct();
        
    }

    // get data pegawai

    function getdata(){
        $this->db->order_by('tgl_pinjam','asc');
        $query = $this->db->get('pinjam_kendaraan',10);
        return $query->result();

    }
    function get_peg_id($params=array()){
        if(isset($params['id'])){
            $this->db->where('id',$params['id']);
        }
        
        $pegawai = $this->db->get('pegawai');
        if(isset($params['id'])){
            $res = $pegawai->row_array();
        }else{
           $res = $pegawai->result_array(); 
        }
        return $res;
    }
    
    function inputdata($data,$table){
        
        $this->db->insert($table,$data);
        
    }

    function editdata($where,$table){
        return $this->db->get_where($table, $where);
    }

    function updatedata($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function view($where,$table){
        $query = $this->db->get_where($table,$where);
        return $query;
    }

    function delete($where,$table){
        $this->db->where($where);
	    $this->db->delete($table);
    }

    
}