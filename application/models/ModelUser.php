<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ModelUser extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array()) {
        if (isset($params['id'])) {
            $this->db->where('user.id', $params['id']);
        }

        if (isset($params['id'])) {
            $this->db->where('user.id', $params['user_id']);
        }

        if (isset($params['role'])) {
            $this->db->where('user.role', $params['role']);
        }

        if (isset($params['username'])) {
            $this->db->where('username', $params['username']);
        }

        if (isset($params['user_image'])) {
            $this->db->where('user_image', $params['user_image']);
        }

        if (isset($params['password'])) {
            $this->db->where('password', $params['password']);
        }

        $this->db->select('id, username, password, role');
        
        $res = $this->db->get('users');

        if (isset($params['id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }



    }
}
