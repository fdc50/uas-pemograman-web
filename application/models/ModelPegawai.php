<?php

class ModelPegawai extends CI_Model{
    function __construct(){
        parent::__construct();
        
    }

    // get data pegawai

    function getdata(){
        $this->db->order_by('nama','asc');
        $query = $this->db->get('pegawai',10);
        return $query->result();

    }
    function get_dep_id($params=array()){
        if(isset($params['id'])){
            $this->db->where('id',$params['id']);
        }
        
        $departemen = $this->db->get('departemen');
        if(isset($params['id'])){
            $res = $departemen->row_array();
        }else{
           $res = $departemen->result_array(); 
        }
        return $res;
    }
    
    function inputdata($data,$table){
        
        $this->db->insert($table,$data);
        
    }

    function editdata($where,$table){
        return $this->db->get_where($table, $where);
    }

    function updatedata($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function view($where,$table){
        $query = $this->db->get_where($table,$where);
        // $query = $this->db->join('pegawai','dep_id.id = nama.id');
        return $query;
    }

    function delete($where,$table){
        $this->db->where($where);
	    $this->db->delete($table);
    }

    
}